const btn = document.querySelector("#btn")

function demChanLe() {
    const soThuNhat = document.querySelector("#so-1").value * 1
    const soThuHai = document.querySelector("#so-2").value * 1
    const soThuBa = document.querySelector("#so-3").value * 1
    const message = document.querySelector(".message")
    let soChan = 0
    let soLe = 0
    if(soThuNhat % 2 == 0) {
        soChan++
    }else {
        soLe++
    }
    if(soThuHai % 2 == 0) {
        soChan++
    }else { 
        soLe++
    }
    if(soThuBa % 2 == 0) {
        soChan++
    }else {
        soLe++
    }

    message.innerText = `có ${soChan} số chẵn và ${soLe} số lẻ`
    message.style.display = "flex"
}



btn.addEventListener("click", function() {
    demChanLe()
})