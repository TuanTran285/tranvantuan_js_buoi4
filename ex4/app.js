const btn = document.querySelector("#btn")

function tamGiac() {
    const doDai1 = document.querySelector("#so-1").value * 1
    const doDai2 = document.querySelector("#so-2").value * 1
    const doDai3 = document.querySelector("#so-3").value * 1
    const message = document.querySelector(".message")
    if(doDai1 + doDai2 > doDai3 && doDai1 + doDai3 > doDai2 && doDai2 + doDai3 > doDai1) {
        if(doDai1 == doDai2 == doDai3) {
            message.innerText = "Hình tam giác đều"
        }else if(doDai1 == doDai2 || doDai1 == doDai3 || doDai2 == doDai3){
            message.innerText = "Hình tam giác cân"
        } else if(Math.pow(doDai1, 2) == Math.pow(doDai2, 2) + Math.pow(doDai3, 2) ||
        Math.pow(doDai2, 2) == Math.pow(doDai1, 2) + Math.pow(doDai3, 2) ||
        Math.pow(doDai3, 2) == Math.pow(doDai1, 2) + Math.pow(doDai2, 2)) {
            message.innerText = "Hinh tam giác vuông"
        } else {
            message.innerText = "Loại tam giác khác"
        }
    }else {
        alert("Dữ liệu không hợp lệ")
    }
    message.style.display = "flex"
} 



btn.addEventListener("click", function() {
    tamGiac()
})