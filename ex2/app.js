const btn = document.querySelector(".submit")

function selectFamily() {
    const formCotrol = document.querySelector("#user-select")
    const message = document.querySelector(".message")
    if(formCotrol.value == "") {
        message.innerText = "Xin chào người lạ!"
    }else if(formCotrol.value == "B") {
        message.innerText = "Xin chào Bố!"
    } else if(formCotrol.value == "M") {
        message.innerText = "Xin chào Mẹ!"
    } else if(formCotrol.value == "A") {
        message.innerText = "Xin chào Anh!"   
    }else if(formCotrol.value == "C") {
        message.innerText = "Xin chào Chị!"   
    }
    message.style.display = "block"
}

btn.addEventListener("click", function(e) {
    selectFamily()
})